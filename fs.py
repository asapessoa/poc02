#from keras import layers
from keras import models
from keras import layers
from keras.models import Sequential
from keras.layers import BatchNormalization, Conv2D, UpSampling2D, MaxPooling2D, Dropout
from keras.optimizers import Adam, SGD
from keras import regularizers
from keras.callbacks import LearningRateScheduler
import numpy as np
import itertools
import pickle
import sys
import glob
import re
import os

from keras.models import load_model
from keras.callbacks import ModelCheckpoint,EarlyStopping,ReduceLROnPlateau

import tensorflow as tf
import keras
#from tensorflow import keras
#from tensorflow.keras import layers


import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (10,10)


class CancelOut(keras.layers.Layer):
    '''
    CancelOut layer, keras implementation. 
    '''
    def __init__(self, activation='sigmoid', cancelout_loss=True, lambda_1=0.002, lambda_2=0.001):
        super(CancelOut, self).__init__()
        self.lambda_1 = lambda_1
        self.lambda_2 = lambda_2
        self.cancelout_loss = cancelout_loss
        
        if activation == 'relu': self.activation = tf.nn.relu
        if activation == 'sigmoid': self.activation = tf.sigmoid
        if activation == 'softmax': self.activation = tf.nn.softmax

    def build(self, input_shape):
        self.w = self.add_weight(
            shape=(input_shape[-1],),
            initializer='ones',
            trainable=True
        )
    def call(self, inputs):
        if self.cancelout_loss:
            self.add_loss( self.lambda_1 * tf.norm(self.w, ord=1) + self.lambda_2 * tf.norm(self.w, ord=2))
        return tf.math.multiply(inputs, self.activation(self.w))
    
    
    def get_config(self):
        return {"activation": self.activation} 


def plot_importance(importances, file_out):
    indices = np.argsort(importances)
    plt.title('Feature Importances')
    plt.barh(range(len(indices)), importances[indices], color='b', align='center')
    plt.yticks(range(len(indices)), [features[i] for i in indices])
    plt.xlabel('Relative Importance')
    plt.savefig(file_out)


def get_unet(nx,ny,nz):
    concat_axis = 3
    inputs = layers.Input(shape = (nx, ny, nz))

    bn = BatchNormalization(axis=3)(inputs)
    co0 = CancelOut(activation='relu')(bn)

    bn0 = BatchNormalization(axis=3)(co0)
    conv1 = layers.Conv2D(32, (3, 3), activation='relu', padding='same', name='conv1_1')(bn0)
    bn1 = BatchNormalization(axis=3)(conv1)
    conv1 = layers.Conv2D(32, (3, 3), activation='relu', padding='same')(bn1)
    bn2 = BatchNormalization(axis=3)(conv1)
    pool1 = layers.MaxPooling2D(pool_size=(2, 2))(bn2)
    conv2 = layers.Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
    bn3 = BatchNormalization(axis=3)(conv2)
    conv2 = layers.Conv2D(64, (3, 3), activation='relu', padding='same')(bn3)
    bn4 = BatchNormalization(axis=3)(conv2)
    pool2 = layers.MaxPooling2D(pool_size=(2, 2))(bn4)

    conv3 = layers.Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
    bn5 = BatchNormalization(axis=3)(conv3)
    conv3 = layers.Conv2D(128, (3, 3), activation='relu', padding='same')(bn5)
    bn6 = BatchNormalization(axis=3)(conv3)
    pool3 = layers.MaxPooling2D(pool_size=(2, 2))(bn6)

    conv4 = layers.Conv2D(256, (3, 3), activation='relu', padding='same')(pool3)
    bn7 = BatchNormalization(axis=3)(conv4)
    conv4 = layers.Conv2D(256, (3, 3), activation='relu', padding='same')(bn7)
    bn8 = BatchNormalization(axis=3)(conv4)
    pool4 = layers.MaxPooling2D(pool_size=(2, 2))(bn8)

    conv5 = layers.Conv2D(512, (3, 3), activation='relu', padding='same')(pool4)
    bn9 = BatchNormalization(axis=3)(conv5)
    conv5 = layers.Conv2D(512, (3, 3), activation='relu', padding='same')(bn9)
    bn10 = BatchNormalization(axis=3)(conv5)

    up_conv5 = layers.UpSampling2D(size=(2, 2))(bn10)
    up6 = layers.concatenate([up_conv5, conv4], axis=concat_axis)
    conv6 = layers.Conv2D(256, (3, 3), activation='relu', padding='same')(up6)
    bn11 = BatchNormalization(axis=3)(conv6)
    conv6 = layers.Conv2D(256, (3, 3), activation='relu', padding='same')(bn11)
    bn12 = BatchNormalization(axis=3)(conv6)

    up_conv6 = layers.UpSampling2D(size=(2, 2))(bn12)
    up7 = layers.concatenate([up_conv6, conv3], axis=concat_axis)
    conv7 = layers.Conv2D(128, (3, 3), activation='relu', padding='same')(up7)
    bn13 = BatchNormalization(axis=3)(conv7)
    conv7 = layers.Conv2D(128, (3, 3), activation='relu', padding='same')(bn13)
    bn14 = BatchNormalization(axis=3)(conv7)

    up_conv7 = layers.UpSampling2D(size=(2, 2))(bn14)
    up8 = layers.concatenate([up_conv7, conv2], axis=concat_axis)
    conv8 = layers.Conv2D(64, (3, 3), activation='relu', padding='same')(up8)
    bn15 = BatchNormalization(axis=3)(conv8)
    conv8 = layers.Conv2D(64, (3, 3), activation='relu', padding='same')(bn15)
    bn16 = BatchNormalization(axis=3)(conv8)

    up_conv8 = layers.UpSampling2D(size=(2, 2))(bn16)
    up9 = layers.concatenate([up_conv8, conv1], axis=concat_axis)
    conv9 = layers.Conv2D(32, (3, 3), activation='relu', padding='same')(up9)
    bn17 = BatchNormalization(axis=3)(conv9)
    conv9 = layers.Conv2D(32, (3, 3), activation='relu', padding='same')(bn17)
    bn18 = BatchNormalization(axis=3)(conv9)

    conv10 = layers.Conv2D(1, (1, 1))(bn18)
    bn19 = BatchNormalization(axis=3)(conv10)

    model = models.Model(inputs=inputs, outputs=conv10)

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='mae', optimizer=sgd, metrics=['mse'])
    #model.compile(loss='mae', optimizer=Adam(lr=0.001), metrics=['mse'])
    print(model.summary())

    return model

def make_input(inputs, year):
    print("[I] Processing the inputs files")
    res = np.load(inputs[0].format(year))
    inputs = np.delete(inputs,0)
    for fin in inputs:
        fin = fin.format(year)
        #print(fin)
        tmp = np.load(fin)

        if tmp.shape[0] == 1:
           tmp = np.moveaxis(tmp,0,1)

        res = np.concatenate((tmp,res), axis = 1)

    res = np.moveaxis(res,1,-1)
    return res

class LearningRateLoggingCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        lr = self.model.optimizer.lr.numpy()
        #tf.summary.scalar('learning rate', data=lr, step=epoch)
        print("[I] LR: {:.2f}; Epoch: {}".format(lr,epoch))



input_patt = ['./dados/{}_Geopotential.npy',
              './dados/Temperature__{}_t.npy',
              './dados/Convective_available_{}_cape.npy',
              './dados/Divergence__{}_d.npy',
              './dados/Relative_humidity_{}_r.npy'
             ]

features = ['Geopotential_l1','Geopotential_l2','Geopotential_l3', 
            'Temperature_l1','Temperature_l2','Temperature_l3',
            'CAPE',
            'Divergence_l1','Divergence_l2','Divergence_l3',
            'Rel_humidity'
             ]

target_patt = './dados/hourly_precipitation_{}.npy'
idxs_patt = './idxs/{}_idxs.npz'

year_ini = 2016
year_fin = 2017


resume_trn = 0
if resume_trn:
   print("[I] Resume mode is ON!")
else:
   print("[I] Resume mode is OFF.")

epochs = 100
batch_size = 32
verb = True
earlystop_mode = 'auto'
earlystop_monitor = 'val_mse'
earlystop_patience = 10
importance_patt = 'importance_{}.png'

first = True
for year in range(year_ini, year_fin+1):
    importance_file = importance_patt.format(year)

    #make the input
    x = make_input(input_patt, year)

    ftarget = target_patt.format(year)
    fidxs = idxs_patt.format(year)

    print("[I] File target: %s" % (ftarget))
    print("[I] File idxs: %s" % (fidxs))

    y = np.load(ftarget)
    y = np.moveaxis(y,1,-1)

     
    xi = 4
    xf = -4
    yi = 4
    yf = -5 
    x = x[:,xi:xf,yi:yf,:]


    xi = 3
    xf = -3
    yi = 3
    yf = -4 
    y = y[:,xi:xf,yi:yf,:]

    nzfile = np.load(fidxs)
    idxs_trn = nzfile['idxs_trn']
    idxs_tst = nzfile['idxs_tst']


    x_train = x[idxs_trn,:,:,:]
    x_test = x[idxs_tst,:,:,:]

    y_train = y[idxs_trn,:,:,:]
    y_test = y[idxs_tst,:,:,:]

    print(x.shape)
    print(y.shape)
    print(idxs_trn.shape)
    print(idxs_tst.shape)
    print(x_train.shape)
    print(x_test.shape)
    print(y_train.shape)
    print(y_test.shape)

    nx = x.shape[1]
    ny = x.shape[2]
    nz = x.shape[3]
    print(nx)
    print(ny)
    print(nz)
    

    if first:
       first = False
       if resume_trn:
          model = load_model(fmodel) 
       else:
          model = get_unet(nx,ny,nz)

    earlystop = EarlyStopping( monitor=earlystop_monitor, patience=earlystop_patience, mode=earlystop_mode, verbose=verb)
    #reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.09, patience=20, min_lr=0.000001)
    lr_callback = LearningRateLoggingCallback()
    callbacks_list = [earlystop, lr_callback]


    #history = model.fit(x_train, y_train, epochs=50, verbose=1, validation_data=(x_test, y_test))
    history = model.fit(x_train, y_train, epochs=epochs, verbose=verb, validation_data=(x_test, y_test), batch_size=batch_size, callbacks=callbacks_list, shuffle=True)
    print('[I] LR: %s' % (model.optimizer.lr.numpy()))

    cancelout_feature_importance_sigmoid = model.get_weights()[0]
    print(cancelout_feature_importance_sigmoid)


    plot_importance(cancelout_feature_importance_sigmoid, importance_file)
    #sys.exit()



#print(x.shape)

sys.exit(0)


